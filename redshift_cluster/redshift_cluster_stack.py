from aws_cdk import (
    core,
    aws_redshift as redshift,
    aws_iam as iam,
    aws_ec2 as ec2,
    aws_secretsmanager as secretsmanager
)

class RedshiftClusterStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, config, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        publicly_accessible = config.get('publicly_accessible')
        allowed_ranges      = config.get('allowed_ranges')

        redshift_secret = secretsmanager.Secret(
            self,
            'my-secret',
            secret_name='redshift-secret',
            generate_secret_string=secretsmanager.SecretStringGenerator(
                exclude_punctuation=True,
                exclude_lowercase=False,
                exclude_numbers=False,
                exclude_uppercase=False,
                include_space=False,
                password_length=12,
                require_each_included_type=True
            )
        )

        redshift_role1 = iam.Role(
            self,
            'my-redshift-role-1',
            role_name='my-redshift-role-1',
            assumed_by=iam.ServicePrincipal(
                service='redshift',
            )
        )

        redshift_role2 = iam.Role(
            self,
            'my-redshift-role-2',
            role_name='my-redshift-role-2',
            assumed_by=iam.ServicePrincipal(
                service='redshift',
            )
        )

        glue_policy_1 = iam.PolicyStatement(
            effect=iam.Effect.ALLOW,
            actions=['glue:*'],
            resources=[f'arn:aws:glue:eu-central-1:{self.account}:catalog'],
        )

        glue_policy_2 = iam.PolicyStatement(
            effect=iam.Effect.ALLOW,
            actions=['glue:*'],
            resources=[f'arn:aws:glue:eu-central-1:{self.account}:catalog'],
        )

        s3_policy_1 = iam.PolicyStatement(
            effect=iam.Effect.ALLOW,
            actions=[
                "s3:List*",
                "s3:Get*"
            ],
            resources=[
                "arn:aws:s3:::my-redshift-bucket-3r4t5y4",
                "arn:aws:s3:::my-redshift-bucket-3r4t5y4/1/*"
            ],
        )

        s3_policy_2 = iam.PolicyStatement(
            effect=iam.Effect.ALLOW,
            actions=[
                "s3:List*",
                "s3:Get*"
            ],
            resources=[
                "arn:aws:s3:::my-redshift-bucket-3r4t5y4",
                "arn:aws:s3:::my-redshift-bucket-3r4t5y4/2/"
            ],
        )

        redshift_role1.add_to_policy(s3_policy_1)
        redshift_role1.add_to_policy(glue_policy_1)

        redshift_role2.add_to_policy(s3_policy_2)
        redshift_role2.add_to_policy(glue_policy_2)

        redshift_vpc = ec2.Vpc(
            self,
            'my-redshift-vpc',
            cidr='20.0.0.0/16',
            max_azs=1,
        )

        redshift_securitygroup = ec2.SecurityGroup(
            self,
            'redshift-securitygroup',
            vpc=redshift_vpc,
            security_group_name='redshift-securitygroup',
        )

        for range in allowed_ranges:
            redshift_securitygroup.add_ingress_rule(
                peer=ec2.Peer.ipv4(range),
                connection=ec2.Port.tcp(5439),
            )


        cluster = redshift.Cluster(
            self,
            'my-redshift-cluster',
            master_user=redshift.Login(
                master_username='admin',
                master_password=redshift_secret.secret_value
            ),
            cluster_name='my-redshift-cluster',
            cluster_type=redshift.ClusterType.SINGLE_NODE,
            default_database_name='dev',
            vpc=redshift_vpc,
            roles=[
                redshift_role1,
                redshift_role2,
            ],
            security_groups=[
                redshift_securitygroup,
            ],
            publicly_accessible=publicly_accessible,
            removal_policy=core.RemovalPolicy.RETAIN
        )
