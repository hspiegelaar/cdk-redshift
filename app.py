#!/usr/bin/env python3

from aws_cdk import core
import yaml

from redshift_cluster.redshift_cluster_stack import RedshiftClusterStack

env = {
    'account': '945241922667',
    'region': 'eu-central-1'
}

with open('config.yaml') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

app = core.App()
RedshiftClusterStack(app, "redshift-cluster", env=env, config=config)

app.synth()
